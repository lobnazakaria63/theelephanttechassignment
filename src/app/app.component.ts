import { Component } from '@angular/core';
import { NavbarService } from './services/navbar.service';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor( public nav: NavbarService, private auth:AuthService ) {}
  title = 'app';
  logout() {
    this.auth.logout();
    
  }
}
