//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {NavbarService} from './services/navbar.service';
import { HomePageComponent } from './components/homePage/home.component';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import {AuthService} from './services/auth.service';
import {CrudService} from './services/crud.service';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DataTableModule,SharedModule} from 'primeng/primeng';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AddCarComponent } from './components/addCar/addCar.component';
import { CarDetailsComponent } from './components/carDetails/carDetails.component';
import {CalendarModule} from 'primeng/components/calendar/calendar';
import { AuthGuard } from './guards/authGuard.guard';
@NgModule({
  declarations: [
    AppComponent,LoginComponent,HomePageComponent,AddCarComponent,CarDetailsComponent
  ],
  imports: [
    BrowserModule,
    CalendarModule,
    AngularFontAwesomeModule,
    DataTableModule,
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
   
     ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path:'homepage', component:HomePageComponent, canActivate:[AuthGuard]},
      {path:'cars/add', component:AddCarComponent ,canActivate:[AuthGuard]},
      {path:'car/:id', component:CarDetailsComponent,canActivate:[AuthGuard]},
      {path:'login', component:LoginComponent},
      {path:"",redirectTo:'login',pathMatch:'full'}
      
    ])
  ],
  providers: [NavbarService,AuthService,CrudService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
