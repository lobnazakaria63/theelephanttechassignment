export interface ICars{
    $key:string;
    brand:string;
    country:ICountry[];
    year:Date;
    image:string;
}
export interface ICountry{
    $key:String;
    country:String;
}