import { Component } from "@angular/core";
import { NavbarService } from "../../services/navbar.service";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { CrudService } from "../../services/crud.service";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ICars, ICountry } from "../../carInterface";

@Component({
    templateUrl:'addCar.component.html'
})
export class AddCarComponent {
    addForm:FormGroup;
    countryList:ICountry[];
    
   constructor(public nav: NavbarService,
        public authService: AuthService,
        private router:Router,
        private _fb: FormBuilder,
        private crudService:CrudService){}

   ngOnInit(){
        setTimeout(() => this.nav.show()) ;
        this.addForm=this._fb.group({
            brand : [''],
            country:[''],
            year :[''],
            image:['']
            });
        
       // this.country.country=this.crudService.getCountry();
       var x= this.crudService.getCountry();
       x.snapshotChanges().subscribe(item => {
           this.countryList=[];
           item.forEach(element =>{
               var y=element.payload.toJSON();
               y['$key']=element.key;
               this.countryList.push(y as ICountry)
           })
       })
    }

   
addCar(){
    debugger
    var data;
    data=this.addForm.value;
    this.addForm.get('year').value
    this.crudService.addCar(data);
    this.router.navigate(['/homepage']);
}
}