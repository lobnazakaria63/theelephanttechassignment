import { Component,OnInit } from "@angular/core";
import { NavbarService } from "../../services/navbar.service";
import { AuthService } from "../../services/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CrudService } from "../../services/crud.service";
import { ICars } from "../../carInterface";
import { AngularFireDatabase } from "angularfire2/database";

@Component({
    templateUrl:'carDetails.component.html'
})
export class CarDetailsComponent {
    car:ICars[];
    key:any;
    constructor(public nav: NavbarService,
        public authService: AuthService,
        private router:Router,
        private crudService:CrudService,
        private route: ActivatedRoute,
        private db: AngularFireDatabase){}

    ngOnInit(){
        this.key = this.route.snapshot.params['id'];
        var x= this.crudService.getData();
        x.snapshotChanges().subscribe(item => {
            this.car=[];
            item.forEach(element =>{
                if(element.key==this.key){
                var y=element.payload.toJSON();
                y['$key']=element.key;
                this.car.push(y as ICars)
                }
            })
        })
       console.log(this.car)
      
        setTimeout(() => this.nav.show()) 
    }
}