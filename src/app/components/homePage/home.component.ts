import { Component,OnInit} from "@angular/core";
import {NavbarService} from '../../services/navbar.service'
import { AuthService } from "../../services/auth.service";
import { CrudService } from "../../services/crud.service";
import { Router } from "@angular/router";
import { ICars } from "../../carInterface";
@Component({
    templateUrl:'home.component.html'
})
export class HomePageComponent {
    key:string;
    carList:ICars[];
    constructor(public nav: NavbarService,
        public authService: AuthService,
        private router:Router,
        private crudService:CrudService){}
 ngOnInit(){
     debugger
   var x= this.crudService.getData();
   x.snapshotChanges().subscribe(item => {
       this.carList=[];
       item.forEach(element =>{
           var y=element.payload.toJSON();
           y['$key']=element.key;
           this.carList.push(y as ICars)
       })
   })
    setTimeout(() => this.nav.show()) 
    
 }
 addCar(){
    this.router.navigate(['/cars/add']);
 }
 deleteCar($key,index:number){
    this.crudService.deleteCar($key);
    this.carList.splice(index,1)
 }

 selectedCar($key){
    this.router.navigate(['/car', $key]);
    this.key=$key;
     
 }

}