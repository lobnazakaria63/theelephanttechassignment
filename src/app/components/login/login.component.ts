import { Component, OnInit } from "@angular/core";
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {emailPattern} from '../../validators';
import {NavbarService} from '../../services/navbar.service';
import { AuthService } from "../../services/auth.service";
@Component({
    templateUrl:'login.component.html',
    styleUrls:['./login.css']
})
export class LoginComponent implements OnInit{
    email:string='';
    password:string='';
    loginForm = new FormGroup({
        email: new FormControl('',[Validators.required,Validators.pattern(emailPattern)]),
        password:new FormControl('',Validators.required)
    })
constructor(public nav: NavbarService,public authService: AuthService){
    
}
ngOnInit() {
   /* solved the issue of " expressionchangedafterithasbeencheckederror: 
   expression has changed after it was checked. previous value:
    'true'. current value: 'false'" by calling hide function of nav inside a setTimeout method*/
   setTimeout(() => this.nav.hide())    
}
debugger
login() {
    this.email=this.loginForm.get('email').value;
    this.password=this.loginForm.get('password').value
    this.authService.login(this.email, this.password);
    this.email = this.password = ''; 
    this.loginForm.reset();   
  }

} 