import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
//import { FirebaseAuth, FirebaseAuthState } from 'angularfire2';  
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';
   
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private auth: AuthService, private router: Router) {}
  
    canActivate(): boolean {
        console.log("OnlyLoggedInUsers");
        if (this.auth.isLoggedIn) { 
          return true;
        } else {
            this.router.navigate(['login']);
          window.alert("You don't have permission to view this page"); 
          
          return false;
        }
      }
    }
  