import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthService {
  isLoggedIn: boolean;
  authState: any = null;
  public user: Observable<firebase.User>;
constructor(private _firebaseAuth: AngularFireAuth, private router: Router) { 
  this._firebaseAuth.authState.subscribe((auth) => {
    this.authState = auth
  });
  }

  login(email: string, password: string) {
    
    this._firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Nice, it worked!');
        this.router.navigate(['homepage']);
        this.isLoggedIn = true;
        console.log(this.isLoggedIn)
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
        this.isLoggedIn = false;
        console.log(this.isLoggedIn)
      });
  }

  logout() {
    this._firebaseAuth
      .auth
      .signOut();
  }
 

    
}