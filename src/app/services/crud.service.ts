import { Injectable } from "@angular/core";
import{ICars} from '../carInterface';
import{ICountry} from '../carInterface';
import {AngularFireDatabase,AngularFireList} from 'angularfire2/database';
import {  FirebaseListObservable } from "angularfire2/database-deprecated";
@Injectable()
export class CrudService {
    carList:AngularFireList<any>;
    countryList:AngularFireList<any>;
    selectedCar:ICars;
 constructor(private firebase:AngularFireDatabase ,){}

 getData(){
     debugger
     this.carList=this.firebase.list('cars');
     return this.carList;
 }
 getCountry(){
     this.countryList=this.firebase.list('countries');
     return this.countryList;
 }


 addCar(car:ICars){
     this.carList.push({
         brand:car.brand,
         country:car.country,
         year:this.formateDate(car),
         image:car.image
     })
 }
 updateCar(car:ICars){
     this.carList.update(car.$key,{
        brand:car.brand,
        country:car.country,
        year:car.year,
        image:car.image
     })
 }

 deleteCar($key:string){
     this.carList.remove($key);
 }

 formateDate(date:ICars):string{
     const year=date.year.getFullYear();
     return `${year}`;
 }

}