// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBeU-ezAOEl2QklaI2BYzTM91eBeTIJ3a0",
    authDomain: "test-demo-f858d.firebaseapp.com",
    databaseURL: "https://test-demo-f858d.firebaseio.com",
    projectId: "test-demo-f858d",
    storageBucket: "test-demo-f858d.appspot.com",
    messagingSenderId: "1065266985164"
  }
};
